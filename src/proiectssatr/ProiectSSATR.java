/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proiectssatr;

import java.util.*;

/**
 *
 * @author adi_g
 */
public class ProiectSSATR{
    public static ArrayList<pasager> pasageri = new ArrayList<>();
    public static ArrayList<statie> statii = new ArrayList<>();
    public static ArrayList<trasee> traseu = new ArrayList<>();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        pasager p1 = new pasager(0, "Doroscan", "Andrei", 10, 0, 0);
        pasager p2 = new pasager(1, "Popescu", "Cristina", 11, 0, 1);
        pasager p3 = new pasager(2, "Sandulache", "George", 12, 0, 2);
        tren tr1 = new tren();
        tren tr2 = new tren();
        tren tr3 = new tren();
        statie s1 = new statie(tr1, 0, "Bacau", 0, "TTRI");
        statie s2 = new statie(tr2, 1, "Cluj-Napoca", 1, "FFKV");
        statie s3 = new statie(tr3, 2, "Bucuresti", 2, "XOCZ");
        bilet b1 = new bilet(0, 70);
        bilet b2 = new bilet(1, 50);
        bilet b3 = new bilet(2, 130);
        trasee t1 = new trasee(0, "Bacau", "Cluj-Napoca", "07.00", "17.30", 50, 0, 70);
        trasee t2 = new trasee(1, "Bacau", "Bucuresti", "10.00", "13.00", 50, 1, 50);
        trasee t3 = new trasee(2, "Cluj-Napoca", "Bucuresti", "06.00", "18.30", 50, 2, 130);
        
        pasageri.add(p1);pasageri.add(p2);pasageri.add(p3);
        statii.add(s1);statii.add(s2);statii.add(s3);
        traseu.add(t1);traseu.add(t2);traseu.add(t3);
        
        meniuPrincipal meniu1 = new meniuPrincipal();
        meniu1.showMainMenu();
    }
    
}
