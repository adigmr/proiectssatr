/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proiectssatr;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;
import javax.swing.*;
import static proiectssatr.ProiectSSATR.pasageri;
import static proiectssatr.ProiectSSATR.statii;
import static proiectssatr.ProiectSSATR.traseu;

/**
 *
 * @author adi_g
 */
public class meniuVizualizeaza implements ActionListener{

    private JButton b1, b2, b3;
    private JFrame f;

    public JButton getButton1() {
        return b1;
    }
    public JButton getButton2() {
        return b2;
    }
    public JButton getButton3() {
        return b3;
    }
    public void setButton1(JButton b1) {
        this.b1 = b1;
    }
    public void setButton2(JButton b2) {
        this.b1 = b2;
    }
    public void setButton3(JButton b3) {
        this.b1 = b3;
    }
    public JFrame getFrame() {
        return f;
    }
    public void setFrame(JFrame f) {
        this.f = f;
    }

    /**
     *
     */
    public void meniuVizualizeazaFrame() {
        System.out.println("Alege una dintre optiuni:");
        System.out.println("1. Vezi pasagerii");
        System.out.println("2. Vezi statiile");
        System.out.println("3. Vezi traseele");
        JFrame f1 = new JFrame("Vizualizeaza");
        b1 = new JButton("1.");
        b2 = new JButton("2.");
        b3 = new JButton("3.");
        b1.setBounds(25, 100, 50, 50);
        b2.setBounds(75, 100, 50, 50);
        b3.setBounds(125, 100, 50, 50);
        b1.addActionListener(this);
        b2.addActionListener(this);
        b3.addActionListener(this);
        f1.add(b1);
        f1.add(b2);
        f1.add(b3);
        f1.setSize(265, 270);
        f1.setLayout(null);
        f1.setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == b1) {
            //Vezi pasageri
            for (pasager p : pasageri) {
                Scanner in = new Scanner(System.in);
                
                System.out.println("ID Pasager: " + p.pasagerid + " | Nume: " + p.nume + " | Prenume: " + p.prenume + " | Loc: " + p.loc + " | Traseu: " + p.traseuid + " | Bilet: " + p.biletid);
            }
        } else {
            if (e.getSource() == b2) {
                //Vezi statii
                for (statie s : statii) {
                    Scanner in = new Scanner(System.in);

                    int tid = s.tren.getTrenid();
                    String tcod = s.tren.getCod();
                    
                    System.out.println("ID Statie: " + s.statieid + " | Denumire: " + s.denumire + " | ID Tren: " + tid + " | Cod tren: " + tcod);
                }
            } else //Vezi trasee
                if(e.getSource()==b3){
            {
                for (trasee t : traseu) {
                    Scanner in = new Scanner(System.in);

                    System.out.println("ID Traseu: " + t.traseuid + " | Plecare: " + t.plecare + " | Sosire: " + t.sosire + " | Timp plecare: " + t.tplecare + " | Timp sosire:" + t.sosire + " | Locuri: " + t.locuri + " | ID Bilet: " + t.biletid + " | Pret: " + t.pret);
                }
            }}
        }
    }
}
