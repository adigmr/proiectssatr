/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proiectssatr;

/**
 *
 * @author adi_g
 */
public class trasee extends bilet {
    public int traseuid;
    public String plecare;
    public String sosire;
    public String tplecare;
    public String tsosire;
    public int locuri;
    
    public trasee(int traseuid, String plecare, String sosire, String tplecare, String tsosire, int locuri, int biletid, float pret){
        super(biletid, pret);
        this.traseuid = traseuid;
        this.plecare = plecare;
        this.sosire = sosire;
        this.tplecare = tplecare;
        this.tsosire = tsosire;
        this.locuri = locuri;
    }
}
