/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proiectssatr;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;
import javax.swing.*;
import static proiectssatr.ProiectSSATR.pasageri;
import static proiectssatr.ProiectSSATR.statii;
import static proiectssatr.ProiectSSATR.traseu;

/**
 *
 * @author adi_g
 */
public class meniuAdauga implements ActionListener{
    
    private JButton b1, b2, b3;
    private JFrame f;

    public JButton getButton1() {
        return b1;
    }
    public JButton getButton2() {
        return b2;
    }
    public JButton getButton3() {
        return b3;
    }
    public void setButton1(JButton b1) {
        this.b1 = b1;
    }
    public void setButton2(JButton b2) {
        this.b1 = b2;
    }
    public void setButton3(JButton b3) {
        this.b1 = b3;
    }
    public JFrame getFrame() {
        return f;
    }
    public void setFrame(JFrame f) {
        this.f = f;
    }

    public void meniuAdaugaFrame() {
        System.out.println("Alege una dintre optiuni:");
        System.out.println("1. Adauga pasageri");
        System.out.println("2. Adauga statii");
        System.out.println("3. Adauga trasee");
        JFrame f2 = new JFrame("Adauga");
        b1 = new JButton("1.");
        b2 = new JButton("2.");
        b3 = new JButton("3.");
        b1.setBounds(25, 100, 50, 50);
        b2.setBounds(75, 100, 50, 50);
        b3.setBounds(125, 100, 50, 50);
        b1.addActionListener(this);
        b2.addActionListener(this);
        b3.addActionListener(this);
        f2.add(b1);
        f2.add(b2);
        f2.add(b3);
        f2.setSize(265, 270);
        f2.setLayout(null);
        f2.setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == b1) {
            // Adauga pasageri
            Scanner in = new Scanner(System.in);

            System.out.println("ID Pasager:");
            int pid = in.nextInt();
            System.out.println("Nume:");
            in.nextLine();
            String pnum = in.nextLine();
            System.out.println("Prenume:");
            String ppnum = in.nextLine();
            System.out.println("Nr. loc:");
            int ploc = in.nextInt();
            System.out.println("ID Traseu:");
            int ptrs = in.nextInt();
            System.out.println("ID Bilet:");
            int pblt = in.nextInt();

            pasageri.add(new pasager(pid, pnum, ppnum, ploc, ptrs, pblt));
            
            System.out.println("Ati adaugat pasagerul cu ID-ul " + pid + ".");
        } else {
            if (e.getSource() == b2) {
                //Adauga statie
                Scanner in = new Scanner(System.in);

                System.out.println("Adaugati tren. ID Tren:");
                int tid = in.nextInt();
                System.out.println("Cod tren:");
                in.nextLine();
                String tcod = in.nextLine();
                tren trn = new tren();
                //trn.setTrenid(tid);
                //trn.setCod(tcod);

                System.out.println("ID Statie:");
                int sid = in.nextInt();
                System.out.println("Denumirea statiei:");
                in.nextLine();
                String snum = in.nextLine();

                statii.add(new statie(trn, tid, tcod, sid, snum));
                
                System.out.println("Ati adaugat statia cu ID-ul " + sid + ".");
            } else { if (e.getSource() == b3){
                //Adauga traseu
                Scanner in = new Scanner(System.in);

                System.out.println("ID Traseu:");
                int trid = in.nextInt();
                System.out.println("Statia de plecare:");
                in.nextLine();
                String trpl = in.nextLine();
                System.out.println("Statie de sosire:");
                String trss = in.nextLine();
                System.out.println("Timpul de plecare:");
                String trtp = in.nextLine();
                System.out.println("Timpul de sosire:");
                String trts = in.nextLine();
                System.out.println("Locuri disponibile:");
                int trloc = in.nextInt();

                System.out.println("ID Bilet:");
                int bid = in.nextInt();
                System.out.println("Pret bilet:");
                float bpret = in.nextFloat();

                traseu.add(new trasee(trid, trpl, trss, trtp, trts, trloc, bid, bpret));
                
                System.out.println("Ati adaugat traseul cu ID-ul " + trid + ".");}
            }
        }
    }
}
