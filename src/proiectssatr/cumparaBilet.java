/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proiectssatr;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;
import javax.swing.*;
import static proiectssatr.ProiectSSATR.pasageri;
import static proiectssatr.ProiectSSATR.traseu;
/**
 *
 * @author adi_g
 */
public class cumparaBilet implements ActionListener{
    private JButton b1, b2;
    private JFrame f;
    private JTextField tf1, tf2;
    private int id;

    public JButton getButton1() {
        return b1;
    }
    public JButton getButton2() {
        return b2;
    }
    public void setButton1(JButton b1) {
        this.b1 = b1;
    }
    public void setButton2(JButton b2) {
        this.b1 = b2;
    }
    public JFrame getFrame() {
        return f;
    }

    public void setFrame(JFrame f) {
        this.f = f;
    }
    public JTextField getField1(){
        return tf1;
    }
    public JTextField getField2(){
        return tf2;
    }
    public void setField1(JTextField tf1){
        this.tf1=tf1;
    }
    public void setField2(JTextField tf2){
        this.tf2=tf2;
    }
    
    public void meniuCauta() {
        System.out.println("Cauta o cursa");
        JFrame f4 = new JFrame("Cauta cursa");
        tf1 = new JTextField();
        tf1.setBounds(30, 50, 150, 20);
        tf2 = new JTextField();
        tf2.setBounds(30, 100, 150, 20);
        b1 = new JButton("Cauta");
        b1.setBounds(30, 200, 120, 40);
        b2 = new JButton("Cumpara");
        b2.setBounds(155, 200, 120, 40);
        b1.addActionListener(this);
        b2.addActionListener(this);
        f4.add(tf1);
        f4.add(tf2);
        f4.add(b1);
        f4.add(b2);
        f4.setSize(340, 300);
        f4.setLayout(null);
        f4.setVisible(true);
    }

    public int cauta(String x, String y) {
        for (int i = 0; i < traseu.size(); i++) {
            for (int j = 0; j < traseu.size(); j++) {
                if (x.equals(traseu.get(i).plecare) && y.equals(traseu.get(j).sosire)) {
                    System.out.println("Am gasit cursa: " + traseu.get(i).traseuid + " | Plecare: " + traseu.get(i).plecare + " | Sosire: " + traseu.get(j).sosire + " | Timp plecare: " + traseu.get(i).tplecare + " | Timp sosire: " + traseu.get(i).tsosire + " | Locuri disponibile: " + traseu.get(i).locuri + " | Pret: " + traseu.get(i).pret);
                    id = i;
                }
                else System.out.println("Nu am gasit cursa.");
            }
        }
        return traseu.get(id).traseuid;
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == b1) {
            String plecare = tf1.getText();
            String sosire = tf2.getText();
            id = cauta(plecare, sosire);
        } else {
            if (e.getSource() == b2) {
                Scanner in = new Scanner(System.in);
                
                System.out.println("ID Pasager: ");
                int pid = in.nextInt();
                System.out.println("Nume: ");
                in.nextLine();
                String pnum = in.nextLine();
                System.out.println("Prenume: ");
                String ppnum = in.nextLine();
                System.out.println("Nr. loc: ");
                int ploc = in.nextInt();
                System.out.println("ID Bilet:");
                int pblt = in.nextInt();

                pasageri.add(new pasager(pid, pnum, ppnum, ploc, id, pblt));

                System.out.println("Ati adaugat pasagerul cu ID-ul " + pid + ".");
            }
        }
    }
}
