/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proiectssatr;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 *
 * @author adi_g
 */
public class meniuPrincipal implements ActionListener{

    private JButton b1, b2, b3, b4;
    private JFrame f;

    public JButton getButton1() {
        return b1;
    }
    public JButton getButton2() {
        return b2;
    }
    public JButton getButton3() {
        return b3;
    }
    public JButton getButton4() {
        return b4;
    }
    public void setButton1(JButton b1) {
        this.b1 = b1;
    }
    public void setButton2(JButton b2) {
        this.b1 = b2;
    }
    public void setButton3(JButton b3) {
        this.b1 = b3;
    }
    public void setButton4(JButton b4) {
        this.b1 = b4;
    }
    public JFrame getFrame() {
        return f;
    }
    public void setFrame(JFrame f) {
        this.f = f;
    }
  
    public void showMainMenu() {
        System.out.println("Alege una dintre optiuni:");
        System.out.println("1. Vizualizeaza datele existente");
        System.out.println("2. Adauga date");
        System.out.println("3. Modifica date");
        System.out.println("4. Cauta o cursa!");
        meniuPrincipalFrame();
    }

    public void meniuPrincipalFrame() {
        JFrame f = new JFrame("Meniu principal");
        b1 = new JButton("1.");
        b2 = new JButton("2.");
        b3 = new JButton("3.");
        b4 = new JButton("4.");
        b1.setBounds(25, 100, 50, 50);
        b2.setBounds(75, 100, 50, 50);
        b3.setBounds(125, 100, 50, 50);
        b4.setBounds(175, 100, 50, 50);
        b1.addActionListener(this);
        b2.addActionListener(this);
        b3.addActionListener(this);
        b4.addActionListener(this);
        f.add(b1);
        f.add(b2);
        f.add(b3);
        f.add(b4);
        f.setSize(265, 270);
        f.setLayout(null);
        f.setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == b1) {
            meniuVizualizeaza meniu1 = new meniuVizualizeaza();
            meniu1.meniuVizualizeazaFrame();
        } else {
            if (e.getSource() == b2) {
                meniuAdauga meniu2 = new meniuAdauga();
                meniu2.meniuAdaugaFrame();
            } else {
                if (e.getSource() == b3) {
                    meniuModifica meniu3 = new meniuModifica();
                    meniu3.meniuModificaFrame();
                } else {
                    cumparaBilet meniu4 = new cumparaBilet();
                    meniu4.meniuCauta();
                }
            }
        }
    }

}
