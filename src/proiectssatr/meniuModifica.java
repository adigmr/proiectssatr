/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proiectssatr;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;
import javax.swing.*;
import static proiectssatr.ProiectSSATR.pasageri;
import static proiectssatr.ProiectSSATR.statii;
import static proiectssatr.ProiectSSATR.traseu;

/**
 *
 * @author adi_g
 */
public class meniuModifica implements ActionListener{
    
    private JButton b1, b2, b3;
    private JFrame f;

    public JButton getButton1() {
        return b1;
    }
    public JButton getButton2() {
        return b2;
    }
    public JButton getButton3() {
        return b3;
    }
    public void setButton1(JButton b1) {
        this.b1 = b1;
    }
    public void setButton2(JButton b2) {
        this.b1 = b2;
    }
    public void setButton3(JButton b3) {
        this.b1 = b3;
    }
    public JFrame getFrame() {
        return f;
    }

    public void setFrame(JFrame f) {
        this.f = f;
    }

    public void meniuModificaFrame() {
        System.out.println("Alege una dintre optiuni:");
        System.out.println("1. Modifica pasagerii");
        System.out.println("2. Modifica statiile");
        System.out.println("3. Modifica traseele");
        JFrame f3 = new JFrame("Modifica");
        b1 = new JButton("1.");
        b2 = new JButton("2.");
        b3 = new JButton("3.");
        b1.setBounds(25, 100, 50, 50);
        b2.setBounds(75, 100, 50, 50);
        b3.setBounds(125, 100, 50, 50);
        b1.addActionListener(this);
        b2.addActionListener(this);
        b3.addActionListener(this);
        f3.add(b1);
        f3.add(b2);
        f3.add(b3);
        f3.setSize(265, 270);
        f3.setLayout(null);
        f3.setVisible(true);
    }
    
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == b1) {
            //Modifica pasageri
            Scanner in = new Scanner(System.in);
            
            System.out.println("Scrieti ID-ul pasagerului:");
            int i = in.nextInt();
            
            System.out.println("Introduceti noile date pentru pasagerul cu ID-ul " +i);
            System.out.println("ID Pasager: ");
            int pid = in.nextInt();
            System.out.println("Nume: ");
            String pnume = in.nextLine();
            System.out.println("Prenume: ");
            String ppnume = in.nextLine();
            System.out.println("Loc: ");
            int ploc = in.nextInt();
            System.out.println("ID Traseu: ");
            int ptrs = in.nextInt();
            System.out.println("ID Bilet: ");
            int pblt = in.nextInt();

            pasager psg = new pasager(pid, pnume, ppnume, ploc, ptrs, pblt);

            pasageri.set(i, psg);

        } else {
            if (e.getSource() == b2) {
                //Modifica statii
                Scanner in = new Scanner(System.in);

                System.out.println("Scrieti ID-ul statiei:");
                int i = in.nextInt();

                System.out.println("Introduceti noile date pentru statia cu ID-ul " + i);
                System.out.println("ID Statie: ");
                int sid = in.nextInt();
                System.out.println("Denumire: ");
                String snum = in.nextLine();
                System.out.println("ID Tren: ");
                int sidtrn = in.nextInt();
                System.out.println("Cod tren: ");
                String scdtrn = in.nextLine();
                
                tren trn = new tren();
                
                statie stt = new statie(trn, sidtrn, scdtrn, sid, snum);
                
                statii.set(i, stt);
            } else { if (e.getSource()==b3){
                //Modifica traseu
                Scanner in = new Scanner(System.in);

                System.out.println("Scrieti ID-ul traseului:");
                int i = in.nextInt();

                System.out.println("Introduceti noile date pentru traseul cu ID-ul " + i);
                System.out.println("ID Traseu: ");
                int tid = in.nextInt();
                System.out.println("Plecare: ");
                String tp = in.nextLine();
                System.out.println("Sosire: ");
                String ts = in.nextLine();
                System.out.println("Timp plecare: ");
                String ttp = in.nextLine();
                System.out.println("Timp sosire: ");
                String tts = in.nextLine();
                System.out.println("Locuri disponibile: ");
                int tloc = in.nextInt();
                System.out.println("ID Bilet: ");
                int tidb = in.nextInt();
                System.out.println("Pret bilet: ");
                float tpretb = in.nextFloat();
                
                bilet blt = new bilet(tidb,tpretb);
                
                trasee trs = new trasee(tid, tp, ts, ttp, tts, tloc, tidb, tpretb);
                
                traseu.set(i, trs);
            }}
        }
    }
}
