/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proiectssatr;


/**
 *
 * @author adi_g
 */
public class statie {
    public tren tren;
    public int statieid;
    public String denumire;
    
    public statie(tren tren, int statieid, String denumire, int trnid, String trncod) {
        this.tren = new tren();
        tren.setTrenid(trnid);
        tren.setCod(trncod);
        this.statieid = statieid;
        this.denumire = denumire;
    }

}
